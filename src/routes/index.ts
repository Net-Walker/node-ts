import { AuthController } from '../controllers/Auth/AuthController';
import { PasswordController } from '../controllers/Auth/PasswordController';
import { PublicController } from '../controllers/PublicController';
import { CustomerController } from '../controllers/CustomerController';
import { CategoryController } from '../controllers/CategoryController';
import { SubCategoryController } from '../controllers/SubCategoryController';
import { ProductController } from '../controllers/PoductController';
import { MerchantController } from '../controllers/MerchantController';
import { OrderController } from '../controllers/OrderController';

import { passportAuth } from '../middleware/auth';
import { Application } from 'express';
import upload from '../config/multer.config';

export class Routes {
  // Controllers
  public authController: AuthController = new AuthController();
  public passwordController: PasswordController = new PasswordController();
  public publicController: PublicController = new PublicController();
  public customerController: CustomerController = new CustomerController();
  public categoryController: CategoryController = new CategoryController();
  public subCategoryController: SubCategoryController = new SubCategoryController();
  public productController: ProductController = new ProductController();
  public merchantController: MerchantController = new MerchantController();
  public orderController: OrderController = new OrderController();
  public baseUrl: string = process.env.BASE_URL || '/api/v1/';

  public routes(app: Application): void {
    app.get(this.baseUrl + 'healthz', this.publicController.healthz);
    app.post(this.baseUrl + 'upload-to-aws', upload.single('file'), this.publicController.upload);

    // Public routes

    // Authentication...
    app.route(this.baseUrl + ':user/register').post(this.authController.register);
    app.route(this.baseUrl + ':user/login').post(this.authController.login);
    app.route(this.baseUrl + ':user/resend').post(this.authController.resendToken);
    app.route(this.baseUrl + ':user/verify/:token').get(this.authController.verify);
    app.route(this.baseUrl + ':user/recover').post(this.passwordController.recover);
    app.route(this.baseUrl + ':user/reset/:token').get(this.passwordController.reset);
    app.route(this.baseUrl + ':user/reset-password/:token').post(this.passwordController.resetPassword);

    //TODO Social Auth Routes

    // Categories
    app.route(this.baseUrl + 'categories').get(this.categoryController.all);
    app.route(this.baseUrl + 'categories/create').post(this.categoryController.create);
    app.route(this.baseUrl + 'categories/update/:id').put(this.categoryController.update);
    app.route(this.baseUrl + 'categories/delete/:id').delete(this.categoryController.delete);

    // Sub Categories...
    app.route(this.baseUrl + 'sub-categories/all').get(this.subCategoryController.index);
    app.route(this.baseUrl + 'sub-categories/create').post(this.subCategoryController.create);
    app.route(this.baseUrl + 'sub-categories/update/:id').put(this.subCategoryController.update);
    app.route(this.baseUrl + 'sub-categories/delete/:id').delete(this.subCategoryController.delete);

    // Products
    app.route(this.baseUrl + 'products/all').get(this.productController.all);

    app.route(this.baseUrl + 'products/create').post(this.productController.create);
    app.route(this.baseUrl + 'products/:id').get(this.productController.findProduct);
    app.route(this.baseUrl + 'products/update/:id').put(this.productController.updateProduct);
    app.route(this.baseUrl + 'products/delete/:id').delete(this.productController.deleteProduct);

    // Orders
    app.route(this.baseUrl + 'orders/all').get(this.orderController.all);

    app.route(this.baseUrl + 'orders/create').post(this.orderController.createOrder);
    app.route(this.baseUrl + 'orders/view/:orderId').get(this.orderController.viewOrder);
    app.route(this.baseUrl + 'change-order-status/:orderId').put(this.orderController.changeProductOrderStatus);
    app.route(this.baseUrl + 'order-status/all').get(this.orderController.allOrderStatus);
    app.route(this.baseUrl + 'order-status/create').post(this.orderController.createOrderStatus);
    app.route(this.baseUrl + 'merchants/all').get(this.merchantController.all);

    // Get user profile based on user group
    app.get(this.baseUrl + ':user/profile', passportAuth, this.authController.profile);

    // protected customer routes

    // protected admin routes
    // ...

    // protected merchant routes
    // ...
  }
}

//TODO refactor all routes to collect two parameter and 3 if middle are is needed.
