import { Request, Response } from 'express';
import { Merchant, MerchantInterface } from '../models/Merchant';

export class MerchantController {

  public all(req: Request, res: Response) {

    const offset = (req.query.offset) ? req.query.offset : 0;
    const limit = (req.query.limit) ? req.query.limit : 15;

    Merchant.findAll<Merchant>({
      offset: Number(offset),
      limit: Number(limit),
    })
      .then((merchants: Merchant[]) => {
        res.json(merchants);
      })
      .catch((err: Error) => {
        res.status(500).json(err);
      });
  }
}
