import { Request, Response } from 'express';
import { Utils } from '../../utils/index';

const utils = new Utils();
export class PasswordController {
  async recover(req: Request, res: Response) {
    const reqUser = req.params.user;
    utils.checkUserGroup(res, reqUser);
    try {
      const { email } = req.body;
      let userModel = utils.getUserModel(reqUser);
      const user = await userModel.findOne({
        where: { email: email },
      });

      if (!user)
        return res.status(401).json({
          message:
            'The email address ' +
            req.body.email +
            ' is not associated with any account. Double-check your email address and try again.',
        });

      //Generate and set password reset token
      user.generatePasswordReset(user);

      // Save the updated user object
      await user.save();

      // send email
      let subject = 'Password change request';
      let to = user.email;
      let from = process.env.FROM_EMAIL;
      let html = `<p>Hi ${user.firstName}</p>
                    <p>Please use this  code ${user.resetPasswordToken} to reset your password.</p> 
                    <p>If you did not request this, please ignore this email and your password will remain unchanged.</p>`;

      await utils.sendEmail({ to, from, subject, html });

      res.status(200).json({
        message: 'A reset email has been sent to ' + user.email + '.',
      });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  }

  async reset(req: Request, res: Response) {
    const reqUser = req.params.user;
    utils.checkUserGroup(res, reqUser);
    try {
      const { token } = req.params;
      let userModel = utils.getUserModel(reqUser);
      const user = await userModel.findOne({
        where: {
          resetPasswordToken: token,
          resetPasswordExpires: { $gt: Date.now() },
        },
      });

      if (!user)
        return res.status(401).json({
          message: 'Password reset token is invalid or has expired.',
        });

      //Redirect user to form with the email address
      res.render('reset', { user });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  }

  async resetPassword(req: Request, res: Response) {
    const reqUser = req.params.user;
    utils.checkUserGroup(res, reqUser);
    try {
      const { token } = req.params;
      let userModel = utils.getUserModel(reqUser);
      const user = await userModel.findOne({
        where: {
          resetPasswordToken: token,
          resetPasswordExpires: { $gt: Date.now() },
        },
      });

      if (!user)
        return res.status(401).json({
          message: 'Password reset token is invalid or has expired.',
        });

      //Set the new password
      user.password = req.body.password;
      user.resetPasswordToken = '';
      user.resetPasswordExpires = 0;
      user.isVerified = true;
      // Save the updated user object
      await user.save();

      let subject = 'Your password has been changed';
      let to = user.email;
      let from = process.env.FROM_EMAIL;
      let html = `<p>Hi ${user.firstName}</p>
                  <p>This is a confirmation that the password for your account ${user.email} has just been changed.</p>`;

      await utils.sendEmail({ to, from, subject, html });

      res.status(200).json({ message: 'Your password has been updated.' });
    } catch (error) {
      res.status(500).json({ message: error.message });
    }
  }
}
