import { Request, Response } from 'express';
import { Product, ProductInterface } from '../models/Product';
import { SubCategory } from '../models/SubCategory';
import { Category } from '../models/Category';
import { Merchant } from '../models/Merchant';

const utils = require('../utils');

export class ProductController {
  public all(req: Request, res: Response) {
    const offset = req.query.offset ? req.query.offset : 0;
    const limit = req.query.limit ? req.query.limit : 15;

    Product.findAll<Product>({
      offset: Number(offset),
      limit: Number(limit),
      include: [
        { model: Category, as: 'categories' },
        { model: SubCategory, as: 'subCategories' },
        { model: Merchant, as: 'merchantInfo' },
      ],
      order: [['createdAt', 'DESC']],
    })
      .then((products: Product[]) => {
        res.json(products);
      })
      .catch((err: Error) => {
        res.status(500).json(err);
      });
  }

  public async create(req: any, res: Response) {
    const params: ProductInterface = req.body;

    // Make Sure all fields are not empty
    if (
      !req.body.name ||
      !req.body.categoryId ||
      !req.body.subCategoryId ||
      !req.body.merchantId ||
      !req.body.specification ||
      !req.body.price ||
      !req.body.description ||
      !req.body.goodsInStock ||
      !req.body.visible ||
      !req.body.approved
    ) {
      res.status(400).json({
        status: false,
        message: 'All Fields are required',
      });
    } else {
      // Check if Category Exist
      const catID = req.body.categoryId;
      const subCatID = req.body.subCategoryId;
      const catExist = await Category.findOne({ where: { id: catID } });
      const subCatExist = await SubCategory.findOne({ where: { id: subCatID } });

      if (catExist === null || subCatExist === null) {
        res.status(400).json({ status: false, message: 'Category or SuCategory does not exist' });
      } else {
        Product.create<Product>(params)
          .then((product: Product) => {
            // Upload Product Image to S3 and record file in database
            res.status(201).json(product);
          })
          .catch((err: Error) => res.status(500).json(err));
      }
    }
  }

  public async findProduct(req: Request, res: Response) {
    const productId = req.params.id;

    if (!productId) {
      res.status(400).json({ status: false, message: 'Product Id is not passed' });
    } else {
      const productIdExist = await Product.findByPk(productId);
      if (productIdExist !== null) {
        Product.findOne<Product>({
          where: { id: productId },
          include: [
            { model: Category, as: 'categories' },
            { model: SubCategory, as: 'subCategories' },
            { model: Merchant, as: 'merchantInfo' },
          ],
        })
          .then((products) => {
            res.json(products);
          })
          .catch((err: Error) => {
            res.status(500).json(err);
          });
      } else {
        res.json({ status: false, message: 'Product ID does not Exist' });
      }
    }
  }

  public updateProduct(req: any, res: Response) {
    const productId = req.params.id;
    const data = req.body;
    Product.findByPk(productId)
      .then((product) => {
        // Check if product was found
        if (!product) {
          return res.json({ status: false, message: 'Product not found' });
        }

        // Check if a data as passed
        if (utils.isEmptyObject(data)) {
          return res.json({ status: false, message: 'Please provide some data' });
        }

        // Remove the Id Key from the data passed
        if (data.id) {
          delete data.id;
        }

        // Update the data to database
        product
          .update(data)
          .then(() => {
            console.log(data);
            res.status(200).json({
              status: true,
              message: 'Product Updated successfully',
              info: data,
            });
          })
          .catch((err) => {
            res.json({ err });
          });
      })
      .catch((err) => {
        res.status(500).json({
          err,
        });
      });
  }

  public deleteProduct(req: Request, res: Response) {
    const productId = req.params.id;

    Product.findByPk(productId)
      .then((product) => {
        if (!product) {
          return res.json({ status: false, message: 'Product not found' });
        }

        Product.destroy({
          where: {
            id: productId,
          },
        })
          .then(() => {
            res.json({ status: true, message: 'Product deleted successfully' });
          })
          .catch((err) => {
            res.json({ err });
          });
      })
      .catch((err) => {
        return res.status(500).json({
          err,
        });
      });
  }
}
