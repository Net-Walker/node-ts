import { Request, Response } from 'express';
import { Customer } from '../models/Customer';

export class CustomerController {
  public index(req: Request, res: Response) {
    const offset = req.query.offset ? req.query.offset : 0;
    const limit = req.query.limit ? req.query.limit : 15;

    Customer.findAll<Customer>({
      offset: Number(offset),
      limit: Number(limit),
      order: [['createdAt', 'DESC']],
    })
      .then((customers: Customer[]) => res.json(customers))
      .catch((err: Error) => res.status(500).json(err));
  }

  public update() {}
  public destroy() {}
}
