import { Request, Response } from 'express';
import { Utils } from '../utils/index';

import stream from 'stream';
import s3 from '../config/s3.config.js';

const utils = new Utils();
export class PublicController {
  healthz(req: Request, res: Response) {
    res.json('We are live and all ok go bcd.ng');
  }

  async upload(req: Request, res: Response) {
    try {
      const s3Client = s3.s3Client;
      const params = s3.uploadParams;

      params.Key = req.file.originalname;
      params.Body = req.file.buffer;

      s3Client.upload(params, (err: string, data: any) => {
        if (err) {
          res.status(500).json({ error: 'Error -> ' + err });
        }
        res.json({ data: data, message: 'File uploaded successfully! -> keyname = ' + req.file.originalname });
      });
    } catch (error) {
      console.error(error);
    }
  }
}
