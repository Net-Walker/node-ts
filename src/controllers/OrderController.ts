import { Request, Response } from 'express';
import { Order, OrderInterface } from '../models/Order';
import { Product } from '../models/Product';
import { Customer } from '../models/Customer';
import { OrderStatus, OrderStatusInterface } from '../models/OrderStatus';
import { Category } from '../models/Category';
import { SubCategory } from '../models/SubCategory';
import { Merchant } from '../models/Merchant';

export class OrderController {
  public all(req: Request, res: Response) {
    const offset = req.query.offset ? req.query.offset : 0;
    const limit = req.query.limit ? req.query.limit : 15;

    Order.findAll<Order>({
      offset: Number(offset),
      limit: Number(limit),
      include: [
        {
          model: Product,
          as: 'product',
          include: [
            { model: Category, as: 'categories' },
            { model: SubCategory, as: 'subCategories' },
            { model: Merchant, as: 'merchantInfo' },
          ],
        },
        { model: Customer, as: 'customer' },
        { model: OrderStatus, as: 'orderStatus' },
      ],
      order: [['createdAt', 'DESC']],
    })
      .then((orders: Order[]) => {
        res.status(200).json({ orders });
      })
      .catch((err) => {
        return res.status(500).json({ err });
      });
  }

  public async viewOrder(req: Request, res: Response) {
    const orderId = req.params.orderId;

    if (!orderId) {
      res.status(400).json({ status: false, message: 'OrderId not passed' });
    } else {
      Order.findOne({
        where: {
          id: orderId,
        },
        include: [
          {
            model: Product,
            as: 'product',
            include: [
              { model: Category, as: 'categories' },
              { model: SubCategory, as: 'subCategories' },
              { model: Merchant, as: 'merchantInfo' },
            ],
          },
          { model: Customer, as: 'customer' },
          { model: OrderStatus, as: 'orderStatus' },
        ],
      })
        .then((order) => {
          res.status(200).json({ order });
        })
        .catch((err) => {
          res.status(500).json({ err, status: false });
        });
    }
  }

  public async createOrder(req: any, res: Response) {
    const params: OrderInterface = req.fields;

    // Check that all fields are filled
    if (
      !req.fields.customerId ||
      !req.fields.productId ||
      !req.fields.agreedPrice ||
      !req.fields.quantity ||
      !req.fields.deliveryInformation ||
      !req.fields.paymentStatus ||
      !req.fields.paymentType ||
      !req.fields.orderType ||
      !req.fields.orderStatusId
    ) {
      res.status(400).json({
        status: false,
        message: 'All fields are required',
      });
    } else {
      const productId = req.fields.productId;
      const customerId = req.fields.customerId;
      const orderStatusId = req.fields.orderStatusId;

      // Make sure that ProductId, CustomerId, OrderStatusId are valid
      const productIdExist = await Product.findOne({ where: { id: productId } });
      const customerIdExist = await Customer.findOne({ where: { id: customerId } });
      const orderStatusIdExist = await OrderStatus.findOne({ where: { id: orderStatusId } });

      let notExist = '';
      if (productIdExist === null) {
        notExist += 'productID, ';
      }
      if (customerIdExist === null) {
        notExist += 'customerId, ';
      }
      if (orderStatusIdExist === null) {
        notExist += 'orderStatusId, ';
      }
      if (productIdExist === null || customerIdExist === null || orderStatusIdExist === null) {
        res.status(400).json({
          status: false,
          message: `${notExist}does not exist`,
        });
      } else {
        // Get the available goods in stock and check if it meets order quantity
        const availableGIS = productIdExist.goodsInStock;
        if (availableGIS === 0) {
          res.json({ status: false, message: 'No Goods In Stock for this product' });
        } else if (req.fields.quantity > availableGIS) {
          res.json({
            status: false,
            message: 'Not enough Goods In Stock for this product',
            quantityDemanded: req.fields.quantity,
            goodsInStock: availableGIS,
          });
        } else {
          const newGIS = availableGIS - req.fields.quantity;
          await Product.update(
            { goodsInStock: newGIS },
            {
              where: {
                id: req.fields.productId,
              },
            },
          );

          // Create Order
          Order.create<Order>(params)
            .then((order: Order) => {
              res.status(201).json(order);
            })
            .catch((err) => {
              res.status(500).json({ err });
            });
        }
      }
    }
  }

  public async changeProductOrderStatus(req: any, res: Response) {
    const orderId = req.params.orderId;
    const orderStatusId = req.fields.orderStatusId;

    if (!orderId || !orderStatusId) {
      res.json({ status: false, message: 'Make sure OrderId and OrderStausId is passed' });
    } else {
      const orderIdExist = await Order.findOne({ where: { id: orderId } });
      const orderStatusIdExist = await OrderStatus.findOne({ where: { id: orderStatusId } });

      let notExist = '';
      if (orderIdExist === null) {
        notExist += 'OrderId, ';
      }
      if (orderStatusIdExist === null) {
        notExist += 'OrderStatusId, ';
      }

      if (orderIdExist === null || orderStatusIdExist === null) {
        res.status(400).json({ status: false, message: `${notExist}does not exist` });
      } else {
        Order.update(
          { orderStatusId },
          {
            where: {
              id: orderId,
            },
          },
        )
          .then((order) => {
            let status;
            switch (orderStatusId) {
              case '1':
                status = 'pending';
                break;
              case '2':
                status = 'processing';
                break;
              case '3':
                status = 'completed';
                break;
              case '4':
                status = 'canceled';
                break;
              default:
                status = 'Unknown';
                break;
            }

            res.json({
              status: true,
              message: `Order status changed successfully to ${status}`,
            });
          })
          .catch((err) => {
            res.status(500).json({ status: false, error: err });
          });
      }
    }
  }

  public async createOrderStatus(req: any, res: Response) {
    const params: OrderStatusInterface = req.fields;

    if (!req.fields.status || !req.fields.description) {
      res.status(400).json({ status: false, message: 'All fields are required' });
    } else {
      const statusExist = await OrderStatus.findOne({ where: { status: req.fields.status } });

      if (statusExist === null) {
        OrderStatus.create(params)
          .then((orderStatus: OrderStatus) => {
            res.status(201).json({ orderStatus });
          })
          .catch((err) => {
            res.status(500).json({ err });
          });
      }
    }
  }

  public allOrderStatus(req: Request, res: Response) {
    const offset = req.query.offset ? req.query.offset : 0;
    const limit = req.query.limit ? req.query.limit : 15;

    OrderStatus.findAll<OrderStatus>({
      offset: Number(offset),
      limit: Number(limit),
      order: [['createdAt', 'DESC']],
    })
      .then((orderStatuses: OrderStatus[]) => {
        res.status(200).json({ orderStatuses });
      })
      .catch((err) => {
        return res.status(500).json({ err });
      });
  }
}
