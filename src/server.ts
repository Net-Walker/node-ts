import app from './app';
const PORT = process.env.PORT || 3000;
// app.use('/api', app);
app.listen(PORT, () => console.log(`Example app listening on port ${PORT}!`));
