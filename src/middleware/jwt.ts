import jwtstrategy from 'passport-jwt';
import extractjwt from 'passport-jwt';

const JwtStrategy = jwtstrategy.Strategy;
const ExtractJwt = extractjwt.ExtractJwt;

import { Customer } from '../models/Customer';
import passport from 'passport';
import { Admin } from '../models/Admin';
import { Merchant } from '../models/Merchant';

const opts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('Bearer'),
  secretOrKey: process.env.JWT_SECRET || 'default103',
};

passport.use(
  new JwtStrategy(opts, (jwt_payload, done) => {
    let User: any;
    if (jwt_payload.userType === 'customer') {
      User = Customer;
    } else if (jwt_payload.userType === 'merchant') {
      User = Merchant;
    } else if (jwt_payload.userType === 'admin') {
      User = Admin;
    } else {
      return done(false, { message: 'Server Error' });
    }
    User.findByPk(jwt_payload.id)
      .then((user: any) => {
        if (user) return done(null, user);
        return done(null, false);
      })
      .catch((err: any) => {
        return done(err, false, { message: 'Server Error' });
      });
  }),
);
