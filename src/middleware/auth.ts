import passport from 'passport';

class Auth {
  public passportAuth(req, res, next) {
    passport.authenticate('jwt', function (err, user, info) {
      if (err) return next(err);
      if (!user)
        return res.status(401).json({
          message: 'Unauthorized Access - Invalid Token Provided!',
        });
      req.user = user;
      next();
    })(req, res, next);
  }
}

export const { passportAuth } = new Auth();
