import express, { Application } from 'express';
import * as bodyParser from 'body-parser';
import { Routes } from './routes/index';
import cookieParser from 'cookie-parser';
import morgan from 'morgan';

import passport from 'passport';
require('dotenv').config();
class App {
  public app: express.Application;
  public routePrv: Routes = new Routes();

  constructor() {
    this.app = express();
    this.config();
    this.routePrv.routes(this.app);
  }

  private config(): void {
    this.app.use(morgan('dev'));
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded({ extended: true }));
    this.app.use(cookieParser());
    this.app.use(passport.initialize());
    require('./middleware/jwt');
  }
}

export default new App().app;
