import { Sequelize, Model, DataTypes } from 'sequelize';
import { database } from '../config/database';
import { MerchantTokenInterface, MerchantToken } from './MerchantToken';
import jwt from 'jsonwebtoken';
import bcrypt from 'bcrypt';
import crypto from 'crypto';

// Merchant Interface
export class Merchant extends Model {
  public id!: number;
  public firstName!: string;
  public lastName!: string;
  public email!: string;
  public password!: string;
  public phone!: string;
  public businessName!: string;
  public rcNumber!: string;
  public location!: string;
  public businessAddress!: string;
  public planId!: number;
  public isVerified!: boolean;
  public resetPasswordToken!: string;
  public resetPasswordExpires!: number;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
  comparePassword!: (password: string) => boolean;
  generateJWT!: () => string;
  generatePasswordReset!: (customer: any) => void;
  generateVerificationToken!: () => MerchantTokenInterface;
}

// Sequelize Model
Merchant.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    firstName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    lastName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    email: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    password: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    phone: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    businessName: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    rcNumber: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    location: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    businessAddress: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    planId: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    isVerified: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
      defaultValue: false,
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn('NOW'),
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn('NOW'),
    },
  },
  {
    tableName: 'merchants',
    sequelize: database,
  },
);

// Merchant.hasMany(Product, {
//   sourceKey: 'id',
//   foreignKey: 'merchantID',
//   as: 'products',
// });

Merchant.sync({ force: false }).then(() => console.log('Merchants table created'));

Merchant.beforeSave((admin) => {
  if (admin.changed('password')) {
    admin.password = bcrypt.hashSync(admin.password, bcrypt.genSaltSync(10));
  }
});

Merchant.prototype.comparePassword = function comparePassword(password: string) {
  return bcrypt.compareSync(password, this.password);
};

Merchant.prototype.generateJWT = function generateJWT() {
  const today = new Date();
  const expirationDate = new Date(today);
  expirationDate.setDate(today.getDate() + 60);

  let payload = {
    id: this.id,
    email: this.email,
    firstName: this.firstName,
    lastName: this.lastName,
    userType: 'merchant',
  };

  return jwt.sign(payload, process.env.JWT_SECRET || '', {
    expiresIn: Math.floor(expirationDate.getTime() / 1000),
  });
};

Merchant.prototype.generatePasswordReset = function generatePasswordReset(admin: MerchantInterface) {
  admin.resetPasswordToken = crypto.randomBytes(3).toString('hex');
  admin.resetPasswordExpires = Date.now() + 3600000; //expires in an hour
};

Merchant.prototype.generateVerificationToken = function generateVerificationToken() {
  let payload = {
    merchantId: this.id,
    token: crypto.randomBytes(3).toString('hex'),
  };

  return new MerchantToken(payload);
};

// Merchant Interface
export interface MerchantInterface {
  firstName: string;
  lastName: string;
  email: string;
  password: string;
  phone: string;
  businessName: string;
  rcNumber: string;
  location: string;
  businessAddress: string;
  planId: number;
  isVerified: string;
  resetPasswordToken: string;
  resetPasswordExpires: number; //complete types
}
