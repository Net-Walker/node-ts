import { Sequelize, Model, DataTypes } from 'sequelize';
import { database } from '../config/database';
import { Merchant } from './Merchant';
import { SubCategory } from './SubCategory';
import { Category } from './Category';

const PROTECTED_ATTRIBUTES = ['password'];

// Product Interface
export class Product extends Model {
  public name!: string;
  public categoryId!: number;
  public subCategoryId!: number;
  public merchantId!: number;
  public specification!: JSON;
  public price!: number;
  public description!: string;
  public goodsInStock!: number;
  public visible!: boolean;
  public approved!: boolean;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;

  toJSON() {
    const attributes = Object.assign({}, this.get());
    for (const a of PROTECTED_ATTRIBUTES) {
      delete attributes[a];
    }
    return attributes;
  }
}

// Sequelize Model
Product.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    name: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    categoryId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      references: {
        model: Category,
        key: 'id',
      },
    },
    subCategoryId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      references: {
        model: SubCategory,
        key: 'id',
      },
    },
    merchantId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      references: {
        model: Merchant,
        key: 'id',
      },
    },
    specification: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    price: {
      type: DataTypes.FLOAT,
      allowNull: false,
    },
    description: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    goodsInStock: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    visible: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    approved: {
      type: DataTypes.BOOLEAN,
      allowNull: false,
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn('NOW'),
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn('NOW'),
    },
  },
  {
    tableName: 'products',
    sequelize: database,
  },
);

// Product.hasOne(SubCategory, {
//   sourceKey: 'subCategoryId',
//   foreignKey: 'id',
//   as: 'subCategories',
// });

// Product.hasOne(Category, {
//   sourceKey: 'categoryId',
//   foreignKey: 'id',
//   as: 'categories',
// });

// Product.hasOne(Merchant, {
//   sourceKey: 'merchantId',
//   foreignKey: 'id',
//   as: 'merchantInfo',
// });

Product.sync({ force: false }).then(() => console.log('Products table created'));

// Product Interface
export interface ProductInterface {
  name: string;
  categoryId: number;
  subCategoryId: number;
  merchantId: number;
  specification: JSON;
  price: number;
  description: string;
  goodsInStock: string;
  visible: boolean;
  approved: boolean;
}
