import { Sequelize, Model, DataTypes } from 'sequelize';
import { database } from '../config/database';

// Wishlist Interface
export class Wishlist extends Model {
  public customerId!: number;
  public productId!: number;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

// Sequelize Model
Wishlist.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    customerId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      // references: {
      //   model: Customer,
      //   key: 'id',
      // },
    },
    productId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
      // references: {
      //   model: Product,
      //   key: 'id',
      // },
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn('NOW'),
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn('NOW'),
    },
  },
  {
    tableName: 'wishlists',
    sequelize: database,
  },
);

Wishlist.sync({ force: false }).then(() => console.log('Wishlist Tables table created'));

// Wishlist Interface
export interface WishlistInterface {
  customerId: number;
  productId: number;
}
