import { Sequelize, Model, DataTypes } from 'sequelize';
import { database } from '../config/database';

// Order Interface
export class Order extends Model {
  public customerId!: number;
  public productId!: number;
  public agreedPrice!: number;
  public quantity!: number;
  public deliveryInformation!: any;
  public paymentStatus!: string;
  public orderType!: string;
  public orderStatusId!: number;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

Order.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    customerId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      // references: {
      //   model: Customer,
      //   key: 'id',
      // },
    },
    productId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      // references: {
      //   model: Product,
      //   key: 'id',
      // },
    },
    agreedPrice: {
      type: DataTypes.FLOAT,
      allowNull: true,
    },
    quantity: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
    },
    deliveryInformation: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    paymentStatus: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    orderType: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    orderStatusId: {
      type: DataTypes.INTEGER,
      allowNull: true,
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn('NOW'),
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn('NOW'),
    },
  },
  {
    tableName: 'orders',
    sequelize: database,
  },
);

// Order.hasOne(Product, {
//   sourceKey: 'productId',
//   foreignKey: 'id',
//   as: 'product',
// });

// Order.hasOne(OrderStatus, {
//   sourceKey: 'orderStatusId',
//   foreignKey: 'id',
//   as: 'orderStatus',
// });

// Order.belongsTo(Customer, {
//   foreignKey: 'customerId',
//   as: 'customer',
// });

Order.sync({ force: false }).then(() => console.log('Orders table created'));

export interface OrderInterface {
  customerId: number;
  productId: number;
  agreedPrice: number;
  quantity: number;
  deliveryInformation: JSON;
  paymentStatus: string;
  paymentType: string; // Bank Transfer, Online Payment
  orderType: string;
  orderStatusId: number;
}
