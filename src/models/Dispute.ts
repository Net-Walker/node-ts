import { Sequelize, Model, DataTypes } from 'sequelize';
import { database } from '../config/database';

// Dispute Interface
export class Dispute extends Model {
  public customerId!: number;
  public merchantId!: number;
  public orderId!: number;
  public complaint!: string;
  public resolved!: boolean;
  public resolution!: string;
  public disputeStatus!: string;
  public readonly createdAt!: Date;
  public readonly updatedAt!: Date;
}

// Sequelize Model
Dispute.init(
  {
    id: {
      type: DataTypes.INTEGER.UNSIGNED,
      autoIncrement: true,
      primaryKey: true,
    },
    customerId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
    },
    merchantId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
    },
    orderId: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: false,
    },
    complaint: {
      type: DataTypes.TEXT,
      allowNull: false,
    },
    resolved: {
      type: DataTypes.BOOLEAN,
      allowNull: true,
    },
    resolution: {
      type: DataTypes.TEXT,
      allowNull: true,
    },
    disputeStatus: {
      type: DataTypes.STRING,
      allowNull: true,
    },
    createdAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn('NOW'),
    },
    updatedAt: {
      allowNull: false,
      type: DataTypes.DATE,
      defaultValue: Sequelize.fn('NOW'),
    },
  },
  {
    tableName: 'disputes',
    sequelize: database,
  },
);

Dispute.sync({ force: false }).then(() => console.log('Disputes table created'));

export interface DisputeInterface {
  customerId: number;
  merchantId: number;
  orderId: number;
  complaint: string;
  resolved: boolean;
  resolution: string;
  disputeStatus: string;
}
