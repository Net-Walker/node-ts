"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.CustomerController = void 0;
const Customer_1 = require("../models/Customer");
class CustomerController {
    index(req, res) {
        const offset = req.query.offset ? req.query.offset : 0;
        const limit = req.query.limit ? req.query.limit : 15;
        Customer_1.Customer.findAll({
            offset: Number(offset),
            limit: Number(limit),
            order: [['createdAt', 'DESC']],
        })
            .then((customers) => res.json(customers))
            .catch((err) => res.status(500).json(err));
    }
    update() { }
    destroy() { }
}
exports.CustomerController = CustomerController;
