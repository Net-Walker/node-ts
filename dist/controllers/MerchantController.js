"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.MerchantController = void 0;
const Merchant_1 = require("../models/Merchant");
class MerchantController {
    all(req, res) {
        const offset = (req.query.offset) ? req.query.offset : 0;
        const limit = (req.query.limit) ? req.query.limit : 15;
        Merchant_1.Merchant.findAll({
            offset: Number(offset),
            limit: Number(limit),
        })
            .then((merchants) => {
            res.json(merchants);
        })
            .catch((err) => {
            res.status(500).json(err);
        });
    }
}
exports.MerchantController = MerchantController;
