"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PasswordController = void 0;
const index_1 = require("../../utils/index");
const utils = new index_1.Utils();
class PasswordController {
    recover(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const reqUser = req.params.user;
            utils.checkUserGroup(res, reqUser);
            try {
                const { email } = req.body;
                let userModel = utils.getUserModel(reqUser);
                const user = yield userModel.findOne({
                    where: { email: email },
                });
                if (!user)
                    return res.status(401).json({
                        message: 'The email address ' +
                            req.body.email +
                            ' is not associated with any account. Double-check your email address and try again.',
                    });
                //Generate and set password reset token
                user.generatePasswordReset(user);
                // Save the updated user object
                yield user.save();
                // send email
                let subject = 'Password change request';
                let to = user.email;
                let from = process.env.FROM_EMAIL;
                let html = `<p>Hi ${user.firstName}</p>
                    <p>Please use this  code ${user.resetPasswordToken} to reset your password.</p> 
                    <p>If you did not request this, please ignore this email and your password will remain unchanged.</p>`;
                yield utils.sendEmail({ to, from, subject, html });
                res.status(200).json({
                    message: 'A reset email has been sent to ' + user.email + '.',
                });
            }
            catch (error) {
                res.status(500).json({ message: error.message });
            }
        });
    }
    reset(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const reqUser = req.params.user;
            utils.checkUserGroup(res, reqUser);
            try {
                const { token } = req.params;
                let userModel = utils.getUserModel(reqUser);
                const user = yield userModel.findOne({
                    where: {
                        resetPasswordToken: token,
                        resetPasswordExpires: { $gt: Date.now() },
                    },
                });
                if (!user)
                    return res.status(401).json({
                        message: 'Password reset token is invalid or has expired.',
                    });
                //Redirect user to form with the email address
                res.render('reset', { user });
            }
            catch (error) {
                res.status(500).json({ message: error.message });
            }
        });
    }
    resetPassword(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const reqUser = req.params.user;
            utils.checkUserGroup(res, reqUser);
            try {
                const { token } = req.params;
                let userModel = utils.getUserModel(reqUser);
                const user = yield userModel.findOne({
                    where: {
                        resetPasswordToken: token,
                        resetPasswordExpires: { $gt: Date.now() },
                    },
                });
                if (!user)
                    return res.status(401).json({
                        message: 'Password reset token is invalid or has expired.',
                    });
                //Set the new password
                user.password = req.body.password;
                user.resetPasswordToken = '';
                user.resetPasswordExpires = 0;
                user.isVerified = true;
                // Save the updated user object
                yield user.save();
                let subject = 'Your password has been changed';
                let to = user.email;
                let from = process.env.FROM_EMAIL;
                let html = `<p>Hi ${user.firstName}</p>
                  <p>This is a confirmation that the password for your account ${user.email} has just been changed.</p>`;
                yield utils.sendEmail({ to, from, subject, html });
                res.status(200).json({ message: 'Your password has been updated.' });
            }
            catch (error) {
                res.status(500).json({ message: error.message });
            }
        });
    }
}
exports.PasswordController = PasswordController;
