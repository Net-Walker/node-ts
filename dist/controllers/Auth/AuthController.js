"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.AuthController = void 0;
const index_1 = require("../../utils/index");
const utils = new index_1.Utils();
class AuthController {
    constructor() {
        this.register = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const reqUser = req.params.user;
            utils.checkUserGroup(res, reqUser);
            try {
                const { email, phone } = req.body;
                let userModel = utils.getUserModel(reqUser);
                // Make sure this account email has not been used already
                const emailExist = yield userModel.findOne({
                    where: { email },
                });
                const phoneExist = yield userModel.findOne({
                    where: { phone },
                });
                if (emailExist) {
                    return res.status(401).json({
                        status: false,
                        message: 'Email address is already associated with another account',
                    });
                }
                if (phoneExist) {
                    return res.status(401).json({
                        status: false,
                        message: 'Phone No is already associated with another account',
                    });
                }
                const user = new userModel(Object.assign(Object.assign({}, req.body), { role: 'basic' }));
                const saveUser = yield user.save();
                if (saveUser) {
                    yield this.sendVerificationEmail(saveUser, res);
                }
            }
            catch (error) {
                res.status(500).json({ status: false, message: error.message });
            }
        });
        this.login = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const reqUser = req.params.user;
            utils.checkUserGroup(res, reqUser);
            try {
                const { email, password } = req.body;
                if (!email || !password) {
                    res.json({ status: false, message: 'Email and password field can not be empty' });
                }
                let userModel = yield utils.getUserModel(reqUser);
                const findUser = yield userModel.findOne({
                    where: { email },
                });
                if (!findUser) {
                    return res.status(401).json({
                        status: false,
                        msg: `The email address ${email} is not associated with any account. Double-check your email address and try again.`,
                    });
                }
                // validate password
                if (!findUser.comparePassword(password))
                    return res.status(401).json({ message: 'Invalid email or password' });
                // Make sure the user has been verified
                if (!findUser.isVerified) {
                    return res.status(401).json({
                        status: false,
                        type: 'not-verified',
                        message: 'Your account has not been verified.',
                    });
                }
                // Login successful, write token, and send back user
                res.status(200).json({
                    status: true,
                    token: findUser.generateJWT(),
                    user: findUser,
                });
            }
            catch (error) {
                res.status(500).json({ message: error.message });
            }
        });
        this.verify = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const reqUser = req.params.user;
            utils.checkUserGroup(res, reqUser);
            if (!req.params.token) {
                return res.status(400).json({
                    status: false,
                    message: 'We were unable to find a user for this token.',
                });
            }
            let userModel = yield utils.getUserModel(reqUser);
            let userModelToken = yield utils.getUserTokenModel(reqUser);
            try {
                // Find a matching token
                const token = yield userModelToken.findOne({
                    where: { token: req.params.token },
                });
                if (!token) {
                    return res.status(400).json({
                        status: false,
                        message: 'We were unable to find a valid token. Your token my have expired.',
                    });
                }
                let userId = yield utils.getUserId(reqUser, token);
                // If we found a token, find a matching user
                userModel
                    .findOne({
                    where: { id: userId },
                })
                    .then((user) => {
                    if (!user) {
                        return res.status(400).json({
                            status: false,
                            message: 'We were unable to find a user for this token.',
                        });
                    }
                    if (user.isVerified) {
                        return res.status(400).json({
                            status: false,
                            message: 'This user has already been verified.',
                        });
                    }
                    // Verify and save the user
                    user.isVerified = true;
                    user
                        .save()
                        .then((result) => {
                        if (result) {
                            return res.status(200).send({ status: true, message: 'The account has been verified. Please log in.' });
                        }
                        return res.status(500).json({ status: false, message: 'An error occured whild verifying users account' });
                    })
                        .catch((e) => {
                        console.error(e);
                    });
                })
                    .catch((e) => {
                    console.error(e);
                });
            }
            catch (error) {
                res.status(500).json({
                    status: false,
                    message: error.message,
                });
            }
        });
        this.resendToken = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const reqUser = req.params.user;
            utils.checkUserGroup(res, reqUser);
            let userModel = utils.getUserModel(reqUser);
            try {
                const { email } = req.body;
                const user = yield userModel.findOne({
                    where: { email },
                });
                if (!user) {
                    return res.status(401).json({
                        status: false,
                        message: `The email address ${req.body.email} is not associated with any account. Double-check your email address and try again.`,
                    });
                }
                if (user.isVerified) {
                    return res.status(400).json({
                        status: false,
                        message: 'This account has already been verified. Please log in.',
                    });
                }
                yield this.sendVerificationEmail(user, res);
            }
            catch (error) {
                res.status(500).json({ status: false, message: error.message });
            }
        });
        this.profile = (req, res) => __awaiter(this, void 0, void 0, function* () {
            const reqUser = req.params.user;
            utils.checkUserGroup(res, reqUser);
            try {
                const id = req.user.id;
                let user = yield utils.getUserData(reqUser, id);
                if (!user) {
                    return res.status(401).json({
                        message: 'User does not exist',
                    });
                }
                res.status(200).json({ user });
            }
            catch (error) {
                res.status(500).json({
                    message: error.message,
                });
            }
        });
    }
    sendVerificationEmail(user, // refactor
    res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const token = user.generateVerificationToken();
                // Save the verification token
                yield token.save();
                const subject = 'Account Verification Token';
                const to = user.email;
                const from = process.env.FROM_EMAIL;
                const html = `<p>Hi ${user.firstName}<p><br>
                    <p>Please use this token <b>${token.token}</b> to verify your account.</p><br>
                    <p>If you did not request this, please ignore this email.</p>`;
                yield utils.sendEmail({ to, from, subject, html });
                res.status(200).json({
                    message: `A verification email has been sent to ${user.email}.`,
                });
            }
            catch (error) {
                res.status(500).json({ message: error.message });
            }
        });
    }
}
exports.AuthController = AuthController;
