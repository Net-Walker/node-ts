"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.ProductController = void 0;
const Product_1 = require("../models/Product");
const SubCategory_1 = require("../models/SubCategory");
const Category_1 = require("../models/Category");
const Merchant_1 = require("../models/Merchant");
const utils = require('../utils');
class ProductController {
    all(req, res) {
        const offset = req.query.offset ? req.query.offset : 0;
        const limit = req.query.limit ? req.query.limit : 15;
        Product_1.Product.findAll({
            offset: Number(offset),
            limit: Number(limit),
            include: [
                { model: Category_1.Category, as: 'categories' },
                { model: SubCategory_1.SubCategory, as: 'subCategories' },
                { model: Merchant_1.Merchant, as: 'merchantInfo' },
            ],
            order: [['createdAt', 'DESC']],
        })
            .then((products) => {
            res.json(products);
        })
            .catch((err) => {
            res.status(500).json(err);
        });
    }
    create(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const params = req.body;
            // Make Sure all fields are not empty
            if (!req.body.name ||
                !req.body.categoryId ||
                !req.body.subCategoryId ||
                !req.body.merchantId ||
                !req.body.specification ||
                !req.body.price ||
                !req.body.description ||
                !req.body.goodsInStock ||
                !req.body.visible ||
                !req.body.approved) {
                res.status(400).json({
                    status: false,
                    message: 'All Fields are required',
                });
            }
            else {
                // Check if Category Exist
                const catID = req.body.categoryId;
                const subCatID = req.body.subCategoryId;
                const catExist = yield Category_1.Category.findOne({ where: { id: catID } });
                const subCatExist = yield SubCategory_1.SubCategory.findOne({ where: { id: subCatID } });
                if (catExist === null || subCatExist === null) {
                    res.status(400).json({ status: false, message: 'Category or SuCategory does not exist' });
                }
                else {
                    Product_1.Product.create(params)
                        .then((product) => {
                        // Upload Product Image to S3 and record file in database
                        res.status(201).json(product);
                    })
                        .catch((err) => res.status(500).json(err));
                }
            }
        });
    }
    findProduct(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const productId = req.params.id;
            if (!productId) {
                res.status(400).json({ status: false, message: 'Product Id is not passed' });
            }
            else {
                const productIdExist = yield Product_1.Product.findByPk(productId);
                if (productIdExist !== null) {
                    Product_1.Product.findOne({
                        where: { id: productId },
                        include: [
                            { model: Category_1.Category, as: 'categories' },
                            { model: SubCategory_1.SubCategory, as: 'subCategories' },
                            { model: Merchant_1.Merchant, as: 'merchantInfo' },
                        ],
                    })
                        .then((products) => {
                        res.json(products);
                    })
                        .catch((err) => {
                        res.status(500).json(err);
                    });
                }
                else {
                    res.json({ status: false, message: 'Product ID does not Exist' });
                }
            }
        });
    }
    updateProduct(req, res) {
        const productId = req.params.id;
        const data = req.body;
        Product_1.Product.findByPk(productId)
            .then((product) => {
            // Check if product was found
            if (!product) {
                return res.json({ status: false, message: 'Product not found' });
            }
            // Check if a data as passed
            if (utils.isEmptyObject(data)) {
                return res.json({ status: false, message: 'Please provide some data' });
            }
            // Remove the Id Key from the data passed
            if (data.id) {
                delete data.id;
            }
            // Update the data to database
            product
                .update(data)
                .then(() => {
                console.log(data);
                res.status(200).json({
                    status: true,
                    message: 'Product Updated successfully',
                    info: data,
                });
            })
                .catch((err) => {
                res.json({ err });
            });
        })
            .catch((err) => {
            res.status(500).json({
                err,
            });
        });
    }
    deleteProduct(req, res) {
        const productId = req.params.id;
        Product_1.Product.findByPk(productId)
            .then((product) => {
            if (!product) {
                return res.json({ status: false, message: 'Product not found' });
            }
            Product_1.Product.destroy({
                where: {
                    id: productId,
                },
            })
                .then(() => {
                res.json({ status: true, message: 'Product deleted successfully' });
            })
                .catch((err) => {
                res.json({ err });
            });
        })
            .catch((err) => {
            return res.status(500).json({
                err,
            });
        });
    }
}
exports.ProductController = ProductController;
