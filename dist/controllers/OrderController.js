"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.OrderController = void 0;
const Order_1 = require("../models/Order");
const Product_1 = require("../models/Product");
const Customer_1 = require("../models/Customer");
const OrderStatus_1 = require("../models/OrderStatus");
const Category_1 = require("../models/Category");
const SubCategory_1 = require("../models/SubCategory");
const Merchant_1 = require("../models/Merchant");
class OrderController {
    all(req, res) {
        const offset = req.query.offset ? req.query.offset : 0;
        const limit = req.query.limit ? req.query.limit : 15;
        Order_1.Order.findAll({
            offset: Number(offset),
            limit: Number(limit),
            include: [
                {
                    model: Product_1.Product,
                    as: 'product',
                    include: [
                        { model: Category_1.Category, as: 'categories' },
                        { model: SubCategory_1.SubCategory, as: 'subCategories' },
                        { model: Merchant_1.Merchant, as: 'merchantInfo' },
                    ],
                },
                { model: Customer_1.Customer, as: 'customer' },
                { model: OrderStatus_1.OrderStatus, as: 'orderStatus' },
            ],
            order: [['createdAt', 'DESC']],
        })
            .then((orders) => {
            res.status(200).json({ orders });
        })
            .catch((err) => {
            return res.status(500).json({ err });
        });
    }
    viewOrder(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const orderId = req.params.orderId;
            if (!orderId) {
                res.status(400).json({ status: false, message: 'OrderId not passed' });
            }
            else {
                Order_1.Order.findOne({
                    where: {
                        id: orderId,
                    },
                    include: [
                        {
                            model: Product_1.Product,
                            as: 'product',
                            include: [
                                { model: Category_1.Category, as: 'categories' },
                                { model: SubCategory_1.SubCategory, as: 'subCategories' },
                                { model: Merchant_1.Merchant, as: 'merchantInfo' },
                            ],
                        },
                        { model: Customer_1.Customer, as: 'customer' },
                        { model: OrderStatus_1.OrderStatus, as: 'orderStatus' },
                    ],
                })
                    .then((order) => {
                    res.status(200).json({ order });
                })
                    .catch((err) => {
                    res.status(500).json({ err, status: false });
                });
            }
        });
    }
    createOrder(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const params = req.fields;
            // Check that all fields are filled
            if (!req.fields.customerId ||
                !req.fields.productId ||
                !req.fields.agreedPrice ||
                !req.fields.quantity ||
                !req.fields.deliveryInformation ||
                !req.fields.paymentStatus ||
                !req.fields.paymentType ||
                !req.fields.orderType ||
                !req.fields.orderStatusId) {
                res.status(400).json({
                    status: false,
                    message: 'All fields are required',
                });
            }
            else {
                const productId = req.fields.productId;
                const customerId = req.fields.customerId;
                const orderStatusId = req.fields.orderStatusId;
                // Make sure that ProductId, CustomerId, OrderStatusId are valid
                const productIdExist = yield Product_1.Product.findOne({ where: { id: productId } });
                const customerIdExist = yield Customer_1.Customer.findOne({ where: { id: customerId } });
                const orderStatusIdExist = yield OrderStatus_1.OrderStatus.findOne({ where: { id: orderStatusId } });
                let notExist = '';
                if (productIdExist === null) {
                    notExist += 'productID, ';
                }
                if (customerIdExist === null) {
                    notExist += 'customerId, ';
                }
                if (orderStatusIdExist === null) {
                    notExist += 'orderStatusId, ';
                }
                if (productIdExist === null || customerIdExist === null || orderStatusIdExist === null) {
                    res.status(400).json({
                        status: false,
                        message: `${notExist}does not exist`,
                    });
                }
                else {
                    // Get the available goods in stock and check if it meets order quantity
                    const availableGIS = productIdExist.goodsInStock;
                    if (availableGIS === 0) {
                        res.json({ status: false, message: 'No Goods In Stock for this product' });
                    }
                    else if (req.fields.quantity > availableGIS) {
                        res.json({
                            status: false,
                            message: 'Not enough Goods In Stock for this product',
                            quantityDemanded: req.fields.quantity,
                            goodsInStock: availableGIS,
                        });
                    }
                    else {
                        const newGIS = availableGIS - req.fields.quantity;
                        yield Product_1.Product.update({ goodsInStock: newGIS }, {
                            where: {
                                id: req.fields.productId,
                            },
                        });
                        // Create Order
                        Order_1.Order.create(params)
                            .then((order) => {
                            res.status(201).json(order);
                        })
                            .catch((err) => {
                            res.status(500).json({ err });
                        });
                    }
                }
            }
        });
    }
    changeProductOrderStatus(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const orderId = req.params.orderId;
            const orderStatusId = req.fields.orderStatusId;
            if (!orderId || !orderStatusId) {
                res.json({ status: false, message: 'Make sure OrderId and OrderStausId is passed' });
            }
            else {
                const orderIdExist = yield Order_1.Order.findOne({ where: { id: orderId } });
                const orderStatusIdExist = yield OrderStatus_1.OrderStatus.findOne({ where: { id: orderStatusId } });
                let notExist = '';
                if (orderIdExist === null) {
                    notExist += 'OrderId, ';
                }
                if (orderStatusIdExist === null) {
                    notExist += 'OrderStatusId, ';
                }
                if (orderIdExist === null || orderStatusIdExist === null) {
                    res.status(400).json({ status: false, message: `${notExist}does not exist` });
                }
                else {
                    Order_1.Order.update({ orderStatusId }, {
                        where: {
                            id: orderId,
                        },
                    })
                        .then((order) => {
                        let status;
                        switch (orderStatusId) {
                            case '1':
                                status = 'pending';
                                break;
                            case '2':
                                status = 'processing';
                                break;
                            case '3':
                                status = 'completed';
                                break;
                            case '4':
                                status = 'canceled';
                                break;
                            default:
                                status = 'Unknown';
                                break;
                        }
                        res.json({
                            status: true,
                            message: `Order status changed successfully to ${status}`,
                        });
                    })
                        .catch((err) => {
                        res.status(500).json({ status: false, error: err });
                    });
                }
            }
        });
    }
    createOrderStatus(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const params = req.fields;
            if (!req.fields.status || !req.fields.description) {
                res.status(400).json({ status: false, message: 'All fields are required' });
            }
            else {
                const statusExist = yield OrderStatus_1.OrderStatus.findOne({ where: { status: req.fields.status } });
                if (statusExist === null) {
                    OrderStatus_1.OrderStatus.create(params)
                        .then((orderStatus) => {
                        res.status(201).json({ orderStatus });
                    })
                        .catch((err) => {
                        res.status(500).json({ err });
                    });
                }
            }
        });
    }
    allOrderStatus(req, res) {
        const offset = req.query.offset ? req.query.offset : 0;
        const limit = req.query.limit ? req.query.limit : 15;
        OrderStatus_1.OrderStatus.findAll({
            offset: Number(offset),
            limit: Number(limit),
            order: [['createdAt', 'DESC']],
        })
            .then((orderStatuses) => {
            res.status(200).json({ orderStatuses });
        })
            .catch((err) => {
            return res.status(500).json({ err });
        });
    }
}
exports.OrderController = OrderController;
