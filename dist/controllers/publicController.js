"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.PublicController = void 0;
const index_1 = require("../utils/index");
const s3_config_js_1 = __importDefault(require("../config/s3.config.js"));
const utils = new index_1.Utils();
class PublicController {
    healthz(req, res) {
        res.json('We are live and all ok go bcd.ng');
    }
    upload(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                const s3Client = s3_config_js_1.default.s3Client;
                const params = s3_config_js_1.default.uploadParams;
                params.Key = req.file.originalname;
                params.Body = req.file.buffer;
                s3Client.upload(params, (err, data) => {
                    if (err) {
                        res.status(500).json({ error: 'Error -> ' + err });
                    }
                    res.json({ data: data, message: 'File uploaded successfully! -> keyname = ' + req.file.originalname });
                });
            }
            catch (error) {
                console.error(error);
            }
        });
    }
}
exports.PublicController = PublicController;
