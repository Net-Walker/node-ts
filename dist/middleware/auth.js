"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.passportAuth = void 0;
const passport_1 = __importDefault(require("passport"));
class Auth {
    passportAuth(req, res, next) {
        passport_1.default.authenticate('jwt', function (err, user, info) {
            if (err)
                return next(err);
            if (!user)
                return res.status(401).json({
                    message: 'Unauthorized Access - Invalid Token Provided!',
                });
            req.user = user;
            next();
        })(req, res, next);
    }
}
exports.passportAuth = new Auth().passportAuth;
