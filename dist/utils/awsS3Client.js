"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.S3Client = void 0;
const fs = require('fs');
const aws_sdk_1 = __importDefault(require("aws-sdk"));
class S3Client {
    constructor(accessKeyId, secretAccessKey) {
        this.client = new aws_sdk_1.default.S3({
            accessKeyId,
            secretAccessKey,
        });
    }
    upload(location, contents) {
        console.log(contents);
        const params = {
            Bucket: location,
            Key: contents.name,
            Body: contents.data,
            ACL: 'public-read',
        };
        return this.client.upload(params, function (s3Err, data) {
            console.log(s3Err);
            if (s3Err)
                return s3Err;
            console.log(`File uploaded successfully at ${data.Location}`);
            return data.Location;
        });
    }
}
exports.S3Client = S3Client;
