"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Dispute = void 0;
const sequelize_1 = require("sequelize");
const database_1 = require("../config/database");
// Dispute Interface
class Dispute extends sequelize_1.Model {
}
exports.Dispute = Dispute;
// Sequelize Model
Dispute.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    customerId: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
    },
    merchantId: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
    },
    orderId: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
    },
    complaint: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
    },
    resolved: {
        type: sequelize_1.DataTypes.BOOLEAN,
        allowNull: true,
    },
    resolution: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: true,
    },
    disputeStatus: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: true,
    },
    createdAt: {
        allowNull: false,
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.Sequelize.fn('NOW'),
    },
    updatedAt: {
        allowNull: false,
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.Sequelize.fn('NOW'),
    },
}, {
    tableName: 'disputes',
    sequelize: database_1.database,
});
Dispute.sync({ force: false }).then(() => console.log('Disputes table created'));
