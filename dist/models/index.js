"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenController = void 0;
const CustomerToken_1 = require("../models/CustomerToken");
const AdminToken_1 = require("../models/AdminToken");
const MerchantToken_1 = require("../models/MerchantToken");
class TokenController {
    constructor() {
        this.initToken = (req, res) => __awaiter(this, void 0, void 0, function* () {
            try {
                const cToken = yield CustomerToken_1.CustomerToken.findAll();
                const mToken = yield MerchantToken_1.MerchantToken.findAll();
                const aToken = yield AdminToken_1.AdminToken.findAll();
            }
            catch (error) {
                res.status(500).json({ status: false, message: error.message });
            }
        });
    }
}
exports.TokenController = TokenController;
