"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Product = void 0;
const sequelize_1 = require("sequelize");
const database_1 = require("../config/database");
const Merchant_1 = require("./Merchant");
const SubCategory_1 = require("./SubCategory");
const Category_1 = require("./Category");
const PROTECTED_ATTRIBUTES = ['password'];
// Product Interface
class Product extends sequelize_1.Model {
    toJSON() {
        const attributes = Object.assign({}, this.get());
        for (const a of PROTECTED_ATTRIBUTES) {
            delete attributes[a];
        }
        return attributes;
    }
}
exports.Product = Product;
// Sequelize Model
Product.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    name: {
        type: sequelize_1.DataTypes.STRING,
        allowNull: false,
    },
    categoryId: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
            model: Category_1.Category,
            key: 'id',
        },
    },
    subCategoryId: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
            model: SubCategory_1.SubCategory,
            key: 'id',
        },
    },
    merchantId: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
            model: Merchant_1.Merchant,
            key: 'id',
        },
    },
    specification: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
    },
    price: {
        type: sequelize_1.DataTypes.FLOAT,
        allowNull: false,
    },
    description: {
        type: sequelize_1.DataTypes.TEXT,
        allowNull: false,
    },
    goodsInStock: {
        type: sequelize_1.DataTypes.INTEGER,
        allowNull: false,
    },
    visible: {
        type: sequelize_1.DataTypes.BOOLEAN,
        allowNull: false,
    },
    approved: {
        type: sequelize_1.DataTypes.BOOLEAN,
        allowNull: false,
    },
    createdAt: {
        allowNull: false,
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.Sequelize.fn('NOW'),
    },
    updatedAt: {
        allowNull: false,
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.Sequelize.fn('NOW'),
    },
}, {
    tableName: 'products',
    sequelize: database_1.database,
});
// Product.hasOne(SubCategory, {
//   sourceKey: 'subCategoryId',
//   foreignKey: 'id',
//   as: 'subCategories',
// });
// Product.hasOne(Category, {
//   sourceKey: 'categoryId',
//   foreignKey: 'id',
//   as: 'categories',
// });
// Product.hasOne(Merchant, {
//   sourceKey: 'merchantId',
//   foreignKey: 'id',
//   as: 'merchantInfo',
// });
Product.sync({ force: false }).then(() => console.log('Products table created'));
