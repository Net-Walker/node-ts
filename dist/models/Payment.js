"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Payment = void 0;
const sequelize_1 = require("sequelize");
const database_1 = require("../config/database");
const Product_1 = require("./Product");
const Merchant_1 = require("./Merchant");
const Order_1 = require("./Order");
class Payment extends sequelize_1.Model {
}
exports.Payment = Payment;
// Sequelize Model
Payment.init({
    id: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
    },
    productId: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
            model: Product_1.Product,
            key: 'id',
        },
    },
    merchantId: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
            model: Merchant_1.Merchant,
            key: 'id',
        },
    },
    orderId: {
        type: sequelize_1.DataTypes.INTEGER.UNSIGNED,
        allowNull: false,
        references: {
            model: Order_1.Order,
            key: 'id',
        },
    },
    transactionId: {
        // Id returned from payment provider
        type: sequelize_1.DataTypes.STRING(128),
        allowNull: false,
    },
    createdAt: {
        allowNull: false,
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.Sequelize.fn('NOW'),
    },
    updatedAt: {
        allowNull: false,
        type: sequelize_1.DataTypes.DATE,
        defaultValue: sequelize_1.Sequelize.fn('NOW'),
    },
}, {
    tableName: 'payments',
    sequelize: database_1.database,
});
Payment.sync({ force: false }).then(() => console.log('Payments table created'));
