"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Routes = void 0;
const CustomerController_1 = require("./controllers/CustomerController");
const CategoryController_1 = require("./controllers/CategoryController");
const SubCategoryController_1 = require("./controllers/SubCategoryController");
const PoductController_1 = require("./controllers/PoductController");
const MerchantController_1 = require("./controllers/MerchantController");
const OrderController_1 = require("./controllers/OrderController");
class Routes {
    constructor() {
        this.customerController = new CustomerController_1.CustomerController();
        this.categoryController = new CategoryController_1.CategoryController();
        this.subCategoryController = new SubCategoryController_1.SubCategoryController();
        this.productController = new PoductController_1.ProductController();
        this.merchantController = new MerchantController_1.MerchantController();
        this.orderController = new OrderController_1.OrderController();
    }
    routes(app) {
        // Customers
        app.route('/customers/all').get(this.customerController.index);
        // Categories
        app.route('/categories').get(this.categoryController.all);
        app.route('/categories/all').get(this.categoryController.index);
        app.route('/categories/create').post(this.categoryController.create);
        app.route('/categories/update/:id').put(this.categoryController.update);
        app.route('/categories/delete/:id').delete(this.categoryController.delete);
        // Sub Categories
        app.route('/sub-categories/all').get(this.subCategoryController.index);
        app.route('/sub-categories/create').post(this.subCategoryController.create);
        app.route('/sub-categories/update/:id').put(this.subCategoryController.update);
        app.route('/sub-categories/delete/:id').delete(this.subCategoryController.delete);
        // Products
        app.route('/products/all').get(this.productController.all);
        app.route('/products/create').post(this.productController.create);
        app.route('/products/:id').get(this.productController.findProduct);
        app.route('/products/update/:id').put(this.productController.updateProduct);
        app.route('/products/delete/:id').delete(this.productController.deleteProduct);
        // Orders
        app.route('/orders/all').get(this.orderController.all);
        app.route('/orders/create').post(this.orderController.createOrder);
        app.route('/orders/view/:orderId').get(this.orderController.viewOrder);
        app.route('/change-order-status/:orderId').put(this.orderController.changeProductOrderStatus);
        // Order Statuses
        app.route('/order-status/all').get(this.orderController.allOrderStatus);
        app.route('/order-status/create').post(this.orderController.createOrderStatus);
        // Merchants
        app.route('/merchants/all').get(this.merchantController.all);
    }
}
exports.Routes = Routes;
