"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.Routes = void 0;
const AuthController_1 = require("../controllers/Auth/AuthController");
const PasswordController_1 = require("../controllers/Auth/PasswordController");
const PublicController_1 = require("../controllers/PublicController");
const CustomerController_1 = require("../controllers/CustomerController");
const CategoryController_1 = require("../controllers/CategoryController");
const SubCategoryController_1 = require("../controllers/SubCategoryController");
const PoductController_1 = require("../controllers/PoductController");
const MerchantController_1 = require("../controllers/MerchantController");
const OrderController_1 = require("../controllers/OrderController");
const auth_1 = require("../middleware/auth");
const multer_config_1 = __importDefault(require("../config/multer.config"));
class Routes {
    constructor() {
        // Controllers
        this.authController = new AuthController_1.AuthController();
        this.passwordController = new PasswordController_1.PasswordController();
        this.publicController = new PublicController_1.PublicController();
        this.customerController = new CustomerController_1.CustomerController();
        this.categoryController = new CategoryController_1.CategoryController();
        this.subCategoryController = new SubCategoryController_1.SubCategoryController();
        this.productController = new PoductController_1.ProductController();
        this.merchantController = new MerchantController_1.MerchantController();
        this.orderController = new OrderController_1.OrderController();
        this.baseUrl = process.env.BASE_URL || '/api/v1/';
    }
    routes(app) {
        app.get(this.baseUrl + 'healthz', this.publicController.healthz);
        app.post(this.baseUrl + 'upload-to-aws', multer_config_1.default.single('file'), this.publicController.upload);
        // Public routes
        // Authentication...
        app.route(this.baseUrl + ':user/register').post(this.authController.register);
        app.route(this.baseUrl + ':user/login').post(this.authController.login);
        app.route(this.baseUrl + ':user/resend').post(this.authController.resendToken);
        app.route(this.baseUrl + ':user/verify/:token').get(this.authController.verify);
        app.route(this.baseUrl + ':user/recover').post(this.passwordController.recover);
        app.route(this.baseUrl + ':user/reset/:token').get(this.passwordController.reset);
        app.route(this.baseUrl + ':user/reset-password/:token').post(this.passwordController.resetPassword);
        //TODO Social Auth Routes
        // Categories
        app.route(this.baseUrl + 'categories').get(this.categoryController.all);
        app.route(this.baseUrl + 'categories/create').post(this.categoryController.create);
        app.route(this.baseUrl + 'categories/update/:id').put(this.categoryController.update);
        app.route(this.baseUrl + 'categories/delete/:id').delete(this.categoryController.delete);
        // Sub Categories...
        app.route(this.baseUrl + 'sub-categories/all').get(this.subCategoryController.index);
        app.route(this.baseUrl + 'sub-categories/create').post(this.subCategoryController.create);
        app.route(this.baseUrl + 'sub-categories/update/:id').put(this.subCategoryController.update);
        app.route(this.baseUrl + 'sub-categories/delete/:id').delete(this.subCategoryController.delete);
        // Products
        app.route(this.baseUrl + 'products/all').get(this.productController.all);
        app.route(this.baseUrl + 'products/create').post(this.productController.create);
        app.route(this.baseUrl + 'products/:id').get(this.productController.findProduct);
        app.route(this.baseUrl + 'products/update/:id').put(this.productController.updateProduct);
        app.route(this.baseUrl + 'products/delete/:id').delete(this.productController.deleteProduct);
        // Orders
        app.route(this.baseUrl + 'orders/all').get(this.orderController.all);
        app.route(this.baseUrl + 'orders/create').post(this.orderController.createOrder);
        app.route(this.baseUrl + 'orders/view/:orderId').get(this.orderController.viewOrder);
        app.route(this.baseUrl + 'change-order-status/:orderId').put(this.orderController.changeProductOrderStatus);
        app.route(this.baseUrl + 'order-status/all').get(this.orderController.allOrderStatus);
        app.route(this.baseUrl + 'order-status/create').post(this.orderController.createOrderStatus);
        app.route(this.baseUrl + 'merchants/all').get(this.merchantController.all);
        // Get user profile based on user group
        app.get(this.baseUrl + ':user/profile', auth_1.passportAuth, this.authController.profile);
        // protected customer routes
        // protected admin routes
        // ...
        // protected merchant routes
        // ...
    }
}
exports.Routes = Routes;
//TODO refactor all routes to collect two parameter and 3 if middle are is needed.
